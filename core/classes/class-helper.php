<?php
namespace Astro_Dynamic;

use Astro_Dynamic\HTML;

class Helper
{

    /**
     * Get animation list
     * @return [array]
     */
    public static function get_animation_in()
    {
        return $animation = array(
            'callout.bounce' => __('Bounce', 'admin_domain'),
            'callout.shake' => __('Shake', 'admin_domain'),
            'callout.flash' => __('Flash', 'admin_domain'),
            'callout.pulse' => __('Pulse', 'admin_domain'),
            'callout.swing' => __('FadeIn', 'admin_domain'),
            'callout.tada' => __('Swing', 'admin_domain'),
            'transition.fadeIn' => __('fadeIn', 'admin_domain'),
            'transition.flipXIn' => __('flipXIn', 'admin_domain'),
            'transition.flipYIn' => __('flipYIn', 'admin_domain'),
            'transition.flipBounceXIn' => __('flipBounceXIn', 'admin_domain'),
            'transition.flipBounceYIn' => __('flipBounceYIn', 'admin_domain'),
            'transition.swoopIn' => __('swoopIn', 'admin_domain'),
            'transition.whirlIn' => __('whirlIn', 'admin_domain'),
            'transition.shrinkIn' => __('shrinkIn', 'admin_domain'),
            'transition.expandIn' => __('expandIn', 'admin_domain'),
            'transition.bounceUpIn' => __('bounceUpIn', 'admin_domain'),
            'transition.bounceDownIn' => __('bounceDownIn', 'admin_domain'),
            'transition.bounceLeftIn' => __('bounceLeftIn', 'admin_domain'),
            'transition.bounceRightIn' => __('bounceRightIn', 'admin_domain'),
            'transition.slideUpIn' => __('slideUpIn', 'admin_domain'),
            'transition.slideDownIn' => __('slideDownIn', 'admin_domain'),
            'transition.slideLeftIn' => __('slideLeftIn', 'admin_domain'),
            'transition.slideRightIn' => __('slideRightIn', 'admin_domain'),
            'transition.slideUpBigIn' => __('slideUpBigIn', 'admin_domain'),
            'transition.slideDownBigIn' => __('slideDownBigIn', 'admin_domain'),
            'transition.slideLeftBigIn' => __('slideLeftBigIn', 'admin_domain'),
            'transition.slideRightBigIn' => __('slideRightBigIn', 'admin_domain'),
            'transition.perspectiveUpIn' => __('perspectiveUpIn', 'admin_domain'),
            'transition.perspectiveDownIn' => __('perspectiveDownIn', 'admin_domain'),
            'transition.perspectiveLeftIn' => __('perspectiveLeftIn', 'admin_domain'),
            'transition.perspectiveRightIn' => __('perspectiveRightIn', 'admin_domain'),
        );
    }

    /**
     * Get animation out list
     * @return [array]
     */
    public static function get_animation_out()
    {
        return $animation = array(
            'callout.bounce' => __('Bounce', 'admin_domain'),
            'callout.shake' => __('Shake', 'admin_domain'),
            'callout.flash' => __('Flash', 'admin_domain'),
            'callout.pulse' => __('Pulse', 'admin_domain'),
            'callout.swing' => __('FadeIn', 'admin_domain'),
            'callout.tada' => __('Swing', 'admin_domain'),
            'transition.fadeOut' => __('fadeOut', 'astro_blog_domain'),
            'transition.flipXOut' => __('flipXOut', 'astro_blog_domain'),
            'transition.flipYOut' => __('flipYOut', 'astro_blog_domain'),
            'transition.flipBounceXOut' => __('flipBounceXOut', 'astro_blog_domain'),
            'transition.flipBounceYOut' => __('flipBounceYOut', 'astro_blog_domain'),
            'transition.swoopOut' => __('swoopOut', 'astro_blog_domain'),
            'transition.whirlOut' => __('whirlOut', 'astro_blog_domain'),
            'transition.shrOutkOut' => __('shrOutkOut', 'astro_blog_domain'),
            'transition.expandOut' => __('expandOut', 'astro_blog_domain'),
            'transition.bounceUpOut' => __('bounceUpOut', 'astro_blog_domain'),
            'transition.bounceDownOut' => __('bounceDownOut', 'astro_blog_domain'),
            'transition.bounceLeftOut' => __('bounceLeftOut', 'astro_blog_domain'),
            'transition.bounceRightOut' => __('bounceRightOut', 'astro_blog_domain'),
            'transition.slideUpOut' => __('slideUpOut', 'astro_blog_domain'),
            'transition.slideDownOut' => __('slideDownOut', 'astro_blog_domain'),
            'transition.slideLeftOut' => __('slideLeftOut', 'astro_blog_domain'),
            'transition.slideRightOut' => __('slideRightOut', 'astro_blog_domain'),
            'transition.slideUpBigOut' => __('slideUpBigOut', 'astro_blog_domain'),
            'transition.slideDownBigOut' => __('slideDownBigOut', 'astro_blog_domain'),
            'transition.slideLeftBigOut' => __('slideLeftBigOut', 'astro_blog_domain'),
            'transition.slideRightBigOut' => __('slideRightBigOut', 'astro_blog_domain'),
            'transition.perspectiveUpOut' => __('perspectiveUpOut', 'astro_blog_domain'),
            'transition.perspectiveDownOut' => __('perspectiveDownOut', 'astro_blog_domain'),
            'transition.perspectiveLeftOut' => __('perspectiveLeftOut', 'astro_blog_domain'),
            'transition.perspectiveRightOut' => __('perspectiveRightOut', 'astro_blog_domain'),
        );
    }

    /**
     * Get social media list
     * @return [array]
     */
    public static function get_social_media()
    {
        return array(
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'youtube' => 'Youtube',
            'google plus' => 'Google plus',
            'rss' => 'RSS',
            'vimeo' => 'Vimeo',
            'pinterest' => 'Pinterest',
            'dribbble' => 'Dribbble',
            'whatsapp' => 'Whatsapp',
            'telegram-plane ' => 'Telegram',
            'wordpress' => 'Wordpress',
            'behance' => 'Behance',
            'dropbox' => 'Dropbox',
            'soundcloud' => 'Soundcloud',
            'tumblr' => 'Tumblr',
            'github' => 'Github',
            'gitlab' => 'Gitlab ',
            'medium' => 'Medium',
            'dribbble ' => 'Dribbble',
            'slack-hash' => 'Slack',
        );
    }

    /**
     * Get terms wp taxonomy
     * @param  [taxonomy name] $term [insert taxonomy name]
     * @return [array]
     */
    public static function get_terms($term, $id = '')
    {
        $terms = array();
        if (!empty($id)) {
            $terms = wp_get_post_terms($id, $term);
        } else {
            $terms = get_terms($term);
        }

        if (!empty($terms) && !is_wp_error($terms)) {
            foreach ($terms as $key => $term) {
                $data[$term->term_id] = $term->name;
            }
        } else {
            $data = array('Not found terms');
        }

        return $data;
    }

    /**
     * Get all post list
     * @param  [post type] $post_type [insert post type]
     * @param  string $perpage   [count post list]
     * @return [array]
     */
    public static function get_posts($post_type, $perpage = '')
    {
        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => $perpage,
        );
        $query = new \WP_Query($args);

        if ($query->have_posts()) {
            while ($query->have_posts()): $query->the_post();

                $data[get_the_id()] = get_the_title();

            endwhile;
        } else {
            $data[] = __('No post result', 'astro_blog_domain');
        }

        return $data;
    }

    /**
     * Get all user
     * @return [array]
     */
    public static function get_user()
    {
        $args = array(
            'orderby' => 'display_name',
        );

        // Create the WP_User_Query object
        $users = get_users();
        foreach ($users as $key => $user) {
            $data[$user->ID] = $user->display_name;
        }

        return $data;
    }

    public static function include_part($template = '')
    {
        include dirname(__FILE__) . '/' . $template . '.php';
    }

    /**
     * Set class attribute with filter hook
     * @param string $filter  [filter hoook]
     * @param array  $classes [classes]
     */
    public static function set_class($filter = '', $classes = array())
    {
        if (!empty($filter)) {
            $class_output = apply_filters($filter, join(' ', array_unique($classes)));
        } else {
            $class_output = join(' ', array_unique($classes));
        }
        return 'class="' . $class_output . '"';
    }

    /**
     * Get an array of all available post type.
     * @return [array]
     */
    public static function get_post_types($post_type = '')
    {
        $items = array();

        // Get the post types.
        $post_types = get_post_types(
            array(
                'public' => true,
            ),
            'objects'
        );

        // add all choose
        if ($post_type) {
            $item[''] = $post_type;
        }

        // Build the array.
        foreach ($post_types as $post_type) {
            $items[$post_type->name] = $post_type->labels->name;
        }
        return $items;
    }

    /**
     * Get image list
     * @return [array]
     */

    public static function get_image_size()
    {

        $data['none'] = 'Hidden Image';
        $data['full'] = 'Original Size';


        foreach (get_intermediate_image_sizes() as $key => $image) {
            $data[$image] = $image;
        }

        return $data;
    }

    /**
     * reuse custom query argument
     * @param array $args ['query argument']
     * @return [array]
     */
    public static function query($args = array())
    {

        // post type
        if (!empty($args['post_type'])) {
            $query_by['post_type'] = $args['post_type'];
        }

        // post per page
        if (!empty($args['posts_per_page'])) {
            $query_by['posts_per_page'] = $args['posts_per_page'];
        }

        // paged
        if (!empty($args['paged'])) {
            $query_by['paged'] = $args['paged'];
        }

        // query by category
        if (!empty($args['query_by']) && $args['query_by'] == 'category') {
            $query_by['cat'] = $args['category'];
        }

        // query by tags
        if (!empty($args['query_by']) && $args['query_by'] == 'tags') {
            $query_by['tag__and'] = $args['tags'];
        }

        // query by manually
        if (!empty($args['query_by']) && $args['query_by'] == 'manually') {
            $query_by['post__in'] = $args['post_id'];
        }

        // query by woocommerce featured product
        if (!empty($args['query_by']) && $query_by['post_type'] == 'product' && $args['query_by'] == 'featured') {
            $query_by['tax_query'][] = array(
                'taxonomy' => 'product_visibility',
                'field' => 'name',
                'terms' => 'featured',
                'operator' => 'IN',
            );
        }

        // query by woocommerce category
        if (!empty($args['query_by']) && $query_by['post_type'] == 'product' && $args['query_by'] == 'category') {
            $query_by['tax_query'][] = array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $args['category'],
                'operator' => 'IN',

            );
        }

        // query by term
        if (!empty($args['query_by']) && $args['query_by'] == 'term') {
            $query_by['tax_query'][] = array(
                'taxonomy' => $args['taxonomy'],
                'field' => 'term_id',
                'terms' => $args['term'],
                'operator' => 'IN',

            );
        }

        // order by general
        if (!empty($args['orderby'])) {
            $query_by['orderby'] = $args['orderby'];
        }

        // order by total sale
        if (!empty($args['orderby']) && $args['orderby'] == 'total_sales') {
            $query_by['meta_key'] = 'total_sales';
            $query_by['orderby'] = 'meta_value_num';
        }

        // order by Most Viewer
        if (!empty($args['orderby']) && $args['orderby'] == 'wp_post_views_count') {
            $query_by['meta_key'] = 'wp_post_views_count';
            $query_by['orderby'] = 'meta_value_num';
        }

        // order by most comment
        if (!empty($args['orderby']) && $args['orderby'] == 'comment_count') {
            $query_by['orderby'] = 'comment_count';
        }

        // order
        if (!empty($args['order'])) {
            $query_by['order'] = $args['order'];
        }

        // offset
        if (!empty($args['offset'])) {
            $query_by['offset'] = $args['offset'];
        }

        // Default Argument
        $query_default = array(
            'post_type' => 'post',
            'posts_per_page' => 5,
            'post_status' => 'publish',
        );

        // Merge Array
        return wp_parse_args($query_by, $query_default);
    }



    // end class
}
