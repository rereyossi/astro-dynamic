 <div class="flex-item">
 
     <div id="testimonial-<?php echo get_the_ID()?>" <?php post_class('adc-testimonial') ?>>


        <div class="adc-testimonial__content">
          <?php echo astro_get_field('content') ?>
        </div>
    

        <div class="adc-testimonial__persona">
            <?php if (has_post_thumbnail() && $settings['image_size'] != 'none'): ?>
              <div class="adc-testimonial__thumbnail rt-img rt-img--full">
                <?php the_post_thumbnail($settings['image_size'], 'img-responsive')?>
              </div>
              <?php endif;?>

         <div class="adc-testimonial__detail">

           <h3 class="adc-testimonial__name"><?php the_title()?></h3>

            <?php if (!empty(astro_get_field('position'))): ?>
            <div  class="adc-testimonial__position">
                <?php echo astro_get_field('position') ?>
            </div>
            <?php endif?>
            
         </div>

        </div>



  </div>

 </div>