<?php $post_classes = !empty($settings['class'])?$settings['class']:""; ?>

<div class="flex-item">
  
    <div id="portfolio-<?php get_the_ID()?>" <?php post_class($post_classes) ?>>

        <?php if (has_post_thumbnail() && $settings['image_size'] != 'none'): ?>
            <div class="adc-portfolio__thumbnail rt-img rt-img--full">
                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail($settings['image_size'], 'img-responsive')?></a>
            </div>
        <?php endif;?>

        <div class="adc-portfolio__body">
            <h3 class="adc-portfolio__title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>

            <?php if ($settings['meta_category']): ?>
            <div class="adc-portfolio__category">
                <?php foreach (get_terms('portfolio-category', get_the_ID()) as $key => $term): ?>
                <a>Design</a>
                <?php endforeach?>
            </div>
            <?php endif?>

        </div>

    </div>
</div>