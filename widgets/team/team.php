<?php
namespace Astro_Dynamic\Elementor;

use Astro_Dynamic\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Team extends \Astro_Dynamic\Elementor_Base
{
    protected $post_type = 'team';
    protected $post_taxonomy = 'team-group';

    public function get_name()
    {
        return 'astro-dynamic-team';
    }

    public function get_title()
    {
        return __('CPT team', 'rt_astro');
    }

    public function get_icon()
    {
        return 'ate-icon ate-team';
    }

    public function get_categories()
    {
        return ['astro-dynamic'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_option();
        $this->setting_pagination();

        $this->style_general();
        $this->style_body();
        $this->style_content();
        $this->style_image();
        $this->style_name();
        $this->style_position();
        $this->style_phone();
        $this->style_email();
        $this->style_social_media();

        // extend global control
        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();

    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'astro_dynamic_domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_type,
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', 'astro_dynamic_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'astro_dynamic_domain'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'astro_dynamic_domain'),
                    'term' => __('Category', 'astro_dynamic_domain'),
                    'manually' => __('Manually Id', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'taxonomy',
            [
                'label' => __('taxonomy', 'astro_dynamic_domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_taxonomy,
            ]
        );

        $this->add_control(
            'term',
            [
                'label' => __('Category', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'default' => 'lastest',
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'term',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Testimonial', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'astro_dynamic_domain'),
                    'wp_post_views_count' => __('Most Viewer', 'astro_dynamic_domain'),
                    'comment_count' => __('Most Review', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'astro_dynamic_domain'),
                    'DESC' => __('DESC', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'astro_dynamic_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'astro_dynamic_domain'),
            ]
        );

        $this->end_controls_section();
    }

    /**
     * options
     */
    protected function setting_option()
    {
        $this->start_controls_section(
            'setting_team',
            [
                'label' => __('Options', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Avatar Size', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'image_position',
            [
                'label' => __('Image Position', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'button',
                'options' => [
                    'left' => __('Left', 'astro_dynamic_domain'),
                    'right' => __('Right', 'astro_dynamic_domain'),
                    'top' => __('Top', 'astro_dynamic_domain'),
                    'button' => __('Buttom', 'astro_dynamic_domain'),
                ],
                'condition' => [
                    'image_size!' => ['none'],
                ],
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro_dynamic_domain'),
                    2 => __(2, 'astro_dynamic_domain'),
                    3 => __(3, 'astro_dynamic_domain'),
                    4 => __(4, 'astro_dynamic_domain'),
                    6 => __(6, 'astro_dynamic_domain'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'label_on' => __('On', 'astro_dynamic_domain'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'setting_desc',
            [
                'label' => __('Description', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro_dynamic_domain'),
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'setting_position',
            [
                'label' => __('Position', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro_dynamic_domain'),
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'setting_phone',
            [
                'label' => __('Phone', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro_dynamic_domain'),
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'setting_email',
            [
                'label' => __('Email', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro_dynamic_domain'),
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'setting_sosmed',
            [
                'label' => __('Social Media', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro_dynamic_domain'),
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            array(
                'name' => 'general_background',
                'selector' => '{{WRAPPER}} .adc-team',
            )
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            array(
                'name' => 'general__border',
                'label' => esc_html__('Border', 'jet-elements'),
                'selector' => '{{WRAPPER}} .adc-team',
            )
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', 'astro_dynamic_domain'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro_dynamic_domain'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro_dynamic_domain'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro_dynamic_domain'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .adc-team' => 'text-align: center;',
                    '{{WRAPPER}} .rt-socmed' => 'justify-content: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_padding',
            [
                'label' => __('Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_body()
    {
        $this->start_controls_section(
            'style_body',
            [
                'label' => __('Body', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'body_padding',
            [
                'label' => __('Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_image()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Width', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'image_border_color',
                'selector' => '{{WRAPPER}} .adc-team__thumbnail',
            ]
        );

        $this->add_responsive_control(
            'image_radius',
            [
                'label' => __('Border Radius', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__thumbnail' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .adc-team__content',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-team__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Content Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();

    }

    public function style_name()
    {
        $this->start_controls_section(
            'style_name',
            [
                'label' => __('Name', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'selector' => '{{WRAPPER}} .adc-team__name',
            ]
        );

        $this->add_control(
            'name_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-team__name' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'name_spacing',
            [
                'label' => __('Name Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();

    }

    public function style_position()
    {
        $this->start_controls_section(
            'style_position',
            [
                'label' => __('Position', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'position_typography',
                'selector' => '{{WRAPPER}} .adc-team__position',
            ]
        );

        $this->add_control(
            'position_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-team__position' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'position_spacing',
            [
                'label' => __('Position Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__position' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_phone()
    {
        $this->start_controls_section(
            'style_phone',
            [
                'label' => __('Phone', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'phone_typography',
                'selector' => '{{WRAPPER}} .adc-team__phone',
            ]
        );

        $this->add_control(
            'phone_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-team__phone' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'phone_spacing',
            [
                'label' => __('Phone Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__phone' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_email()
    {
        $this->start_controls_section(
            'style_email',
            [
                'label' => __('Email', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'email_typography',
                'selector' => '{{WRAPPER}} .adc-team__email',
            ]
        );

        $this->add_control(
            'email_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-team__email' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'email_spacing',
            [
                'label' => __('email Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-team__email' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_social_media()
    {
        $this->start_controls_section(
            'style_social',
            [
                'label' => __('Social', 'elementor'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'social_normal',
            [
                'label' => __('Normal', 'astro_dynamic_domain'),
            ]
        );
        $this->add_control(
            'social_style',
            [
                'label' => __('Color', 'elementor'),
                'type' => Controls_Manager::SELECT,
                'default' => 'brand',
                'options' => [
                    'brand' => __('Official Color', 'elementor'),
                    'border' => __('Border', 'elementor'),
                    'custom' => __('Custom', 'elementor'),
                ],
            ]
        );

        $this->add_control(
            'icon_primary_color',
            [
                'label' => __('Primary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'icon_color' => 'custom',
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'icon_secondary_color',
            [
                'label' => __('Secondary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'icon_color' => 'custom',
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'icon_size',
            [
                'label' => __('Size', 'elementor'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 6,
                        'max' => 300,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'icon_padding',
            [
                'label' => __('Padding', 'elementor'),
                'type' => Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'padding: {{SIZE}}{{UNIT}};',
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
            ]
        );

        $this->add_responsive_control(
            'icon_spacing',
            [
                'label' => __('Spacing', 'elementor'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item:not(:last-child)' => 'margin-right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'image_border', // We know this mistake - TODO: 'icon_border' (for hover control condition also)
                'selector' => '{{WRAPPER}} .rt-socmed__item',
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'border_radius',
            [
                'label' => __('Border Radius', 'elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_tab();

        $this->start_controls_tab(
            'social_hover',
            [
                'label' => __('Hover', 'astro_dynamic_domain'),
            ]
        );
        $this->add_control(
            'social_primary_color_hover',
            [
                'label' => __('Primary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'condition' => [
                    'icon_color' => 'custom',
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-social__item:hover' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'social_secondary_color_hover',
            [
                'label' => __('Secondary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'condition' => [
                    'icon_color' => 'custom',
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-social__item:hover' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'social_border_color_hover',
            [
                'label' => __('Border Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'condition' => [
                    'image_border_border!' => '',
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-social__item:hover' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end social media */

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/team/team-view',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
}
