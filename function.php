<?php

/*=================================================
 * LIMIT THE CUNTENT
/*================================================= */
if (!function_exists('astro_the_content')) {
    function astro_the_content($excerpt_length = '')
    {
        if (empty($excerpt_length)) {
            $excerpt_length = 18;
        }

        $the_excerpt = strip_tags(strip_shortcodes(get_the_content()));
        $words = explode(' ', $the_excerpt, $excerpt_length + 1);

        if (count($words) > $excerpt_length):
            array_pop($words);
            $the_excerpt = implode(' ', $words);
            $the_excerpt .= '...';
        endif;

        echo $the_excerpt;
    }
}

/*=================================================
 * GET POPULAR POSTS
/*================================================= */
/**
 * Create metabox for store count
 * number increment if user visit single post
 */
if (!function_exists('rt_post_popular') && !function_exists('astro_post_popular')) {
    function astro_post_popular($postID)
    {
        if (!is_admin()) {
            $count_key = 'wp_post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if ($count == '') {
                $count = 0;
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '1');
            } else {
                ++$count;
                update_post_meta($postID, $count_key, $count);
            }
        }
    }
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
}

/**
 * add function to single post with the_content filter
 */
if (!function_exists('rt_set_popular_post') && !function_exists('astro_set_popular_post')) {
    function astro_set_popular_post($content)
    {
        if (is_single() && !is_admin()) {
            astro_post_popular(get_the_ID());
        }
        return $content;
    }
    add_filter('the_content', 'astro_set_popular_post', 20);
}

/**
 * get post count number, insert anywhere
 */
if (!function_exists('astro_get_popular_post')) {
    function astro_get_popular_post($postID)
    {
        $count = get_post_meta(get_the_ID(), 'wp_post_views_count', true);
        if ($count) {
            $count = $count;
        } else {
            $count = '0';
        }

        return $count;
    }
}

/*=================================================
 * GET FONTAWESOME 5 CLASSES
/*================================================= */
if (!function_exists('astro_get_fontawesome_class')) {
    function astro_get_fontawesome_class($name)
    {
        $social = array(
            'facebook' => 'facebook-f',
            'vimeo-v' => 'vimeo-v',
            'pinterest' => 'pinterest-p',
            'telegram' => 'telegram-plane',
            'medium' => 'medium-m',
        );

        /** return value by array */
        $data = !empty($social[$name]) ? $social[$name] : $name;

        return 'fa fa-' . $data;
    }
}

/*=================================================;
/* GET FIELD ACF
/*================================================= */
/** This function replace default get field acf */

if (!function_exists('astro_get_field')) {
    function astro_get_field($field, $post_id = false, $format_value = false)
    {

        if (!class_exists('acf')) {
            return false;
        }

        $value = get_field($field, $post_id, $format_value);

        /**
         * Some values are saved as empty string or 0 for fields (e.g true_false fields).
         * So we used is_null instead of is_empty to check if post meta is set or not.
         */
        if (is_null($value)) {
            return false;

        }

        return $value;
    }

}
