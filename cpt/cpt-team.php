<?php
/*=================================================;
/* Post Type: Teams.
/*================================================= */
function rt_register_cpt_team()
{

    /**
     * Post Type: Teams.
     */

    $labels = array(
        "name" => __("Teams", "rt_domain"),
        "singular_name" => __("team", "rt_domain"),
        "featured_image" => __("Profile Picture", "rt_domain"),
        "set_featured_image" => __("Set Profile Picture", "rt_domain"),
        "remove_featured_image" => __("Remove Profile Picture", "rt_domain"),
        "use_featured_image" => __("Use as Profile Picture", "rt_domain"),
    );

    $args = array(
        "label" => __("Teams", "rt_domain"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "team", "with_front" => true),
        "query_var" => true,
        "menu_icon" => "dashicons-businessman",
        "supports" => array("title", "thumbnail"),
    );

    register_post_type("team", $args);
}

add_action('init', 'rt_register_cpt_team');

/*=================================================;
/* GROUP
/*================================================= */
function rt_register_team_group()
{

    /**
     * Taxonomy: Groups.
     */

    $labels = array(
        "name" => __("Groups", "rt_domain"),
        "singular_name" => __("Group", "rt_domain"),
    );

    $args = array(
        "label" => __("Groups", "rt_domain"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'team_group', 'with_front' => true),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "team_group",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy("team_group", array("team"), $args);
}
add_action('init', 'rt_register_team_group');

/*=================================================;
/* TITLE
/*================================================= */
function rt_change_title_team($title)
{
    $screen = get_current_screen();

    if ('team' == $screen->post_type) {
        $title = 'Enter team name';
    }

    return $title;
}

add_filter('enter_title_here', 'rt_change_title_team');
