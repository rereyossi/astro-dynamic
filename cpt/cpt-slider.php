<?php

function rt_register_cpt_slider()
{

    /**
     * Post Type: Slider.
     */

    $labels = array(
        "name" => __("Slider", "rt_domain"),
        "singular_name" => __("Slider", "rt_domain"),
    );

    $args = array(
        "label" => __("Slider", "rt_domain"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "slider", "with_front" => true),
        "query_var" => true,
        "supports" => array("title"),
    );

    register_post_type("slider", $args);
}

add_action('init', 'rt_register_cpt_slider');
