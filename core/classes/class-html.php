<?php
namespace Astro_Dynamic;
class HTML
{

    /**
     * open tag html helper
     * @param  string $tag  [tag html]
     * @param  array  $args [setting]
     * @return [type]       [html]
     */
    public static function open($args = '', $tag = '')
    {
        $tag = !empty($tag) ? $tag : 'div';

        if (is_array($args)) {
            foreach ($args as $key => $attribute) {
                if ($key === 'class') {
                    $data[] = $key . '=' . '"' . join(' ', $attribute) . '"';
                } elseif ($key === 'id') {
                    $data[] = $key . '=' . '"' . $attribute . '"';
                } else {
                    $data[] = 'data-' . $key . '=' . '"' . $attribute . '"';
                }
            }
            $attribute = join(' ', $data);
        } else {
            $attribute = "class='{$args}'";
        }

        return "<{$tag} {$attribute}>";
    }

    /**
     * Close tag helper
     * @param  string $tag  [tag html]
     * @param  string $args [setting]
     * @return [type]       [html]
     */
    public static function close($tag = '', $args = '')
    {
        $tag = !empty($tag) ? $tag : 'div';

        return "</{$tag}>";
    }

    /**
     * open tag slider open
     * @param  array  $args [setting]
     * @return [type]       [html slider]
     */
    public static function before_slider($args = array())
    {
        if (!empty($args['class'])) {
            $classes[] = join(' ', $args['class']);
        }

        if (!empty($args['sync'])) {
            $classes[] = 'rt-slider rt-slider--thumbnail js-astro-slider-sync';
        } else {
            $classes[] = 'rt-slider js-astro-slider';
        }

        $defaults = array(
            'class' => $classes,
            'items-lg' => 4,
            'items-md' => 2,
            'items-sm' => 1,
            'padding' => 1,
            'gap' => 10,
            'loop' => false,
            'autoplay' => true,
            'pagination' => false,
            'nav' => true,
            'lazyLoad' => false,
            'nav-icon-left' => 'fa fa-angle-left',
            'nav-icon-right' => 'fa fa-angle-right',
        );

        $slider = wp_parse_args($args, $defaults);

        return self::open($slider);
    }

    /**
     * Close tag slider
     * @return [type] [html]
     */
    public static function after_slider()
    {
        return "</div>";
    }

    /**
     * Nav slider
     */
    public static function nav_slider()
    {
        echo '<nav class="rt-slider__nav">';
        echo '<a class="rt-slider__prev js-astro-slider-prev"><i class="fa fa-angle-left"></i>' . __('Prev', 'astro_blog_domain') . '</a>';
        echo '<a class="rt-slider__next js-astro-slider-next">' . __('Next', 'astro_blog_domain') . '<i class="fa fa-angle-right"></i></a>';
        echo '</nav>';
    }

    /**
     * Show pagination
     * @param array $args [setting query]
     * @return ['html']
     */
    public static function pagination($args = array())
    {
        $output = '';

        if ($args['style'] == 'number' && $args['style'] != 'no_pagination') {
            $output = '<div class="rt-pagination">' .
            paginate_links(array(
                'prev_text' => __('<span class="ti-arrow-left"></span>', 'rt-domain'),
                'next_text' => __('<span class="ti-arrow-right"></span>', 'rt-domain'),
            ))
                . '</div>';
        }

        if ($args['style'] == 'loadmore' && $args['style'] != 'no_pagination') {
            $output = '<div class="rt-pagination rt-pagination--loadmore">
	              <div class="rt-pagination__spinner js-' . $args['id'] . '-spinner">
	                  <i class="fa fa-spinner fa-spin fa-3x"></i>
	              </div>
	              <a href="#" data-triger-id="' . $args['id'] . '" class="rt-pagination__button js-astro-dynamic-load rt-btn">' . __('Load More', 'astro_blog_domain') . '</a>
	           </div>';
        }

        return $output;
    }

    /**
     * Header block
     * @param  array  $args [setting]
     * @return [type]       [HTML]
     */
    public static function header_block($args = array())
    {
        $id = !empty($args['id']) ? $args['id'] : '';
        $class = !empty($args['class']) ? $args['class'] : '';

        if (!empty($args['title'])) {
            $output = '<div id="' . $id . '" class="rt-header-block ' . $class . '">';
            $output .= '<h2 class="rt-header-block__title">' . $args['title'] . '</h2>';
            if (!empty($args['nav'])):
                $output .= '<div class="rt-header-block__nav">';
                $output .= '<a class="rt-header-block__prev js-astro-slider-prev"><i class="fa fa-angle-left"></i></a>';
                $output .= '<a class="rt-header-block__next js-astro-slider-next"><i class="fa fa-angle-right"></i></a>';
                $output .= '</div>';
            endif;
            $output .= '</div>';

            return $output;
        }
    }

    /**
     * show script tag
     * @param  [type] $args [value]
     * @return [type]       [tag]
     */
    public static function script($args)
    {
        $output = '<script>' . $args . '</script>';

        return $output;
    }
    // end class
}
