<?php
/**
 * Plugin Name: Astro Dynamic
 * Description: Blog Widgets for Elementor page builder.
 * Plugin URI:  https://webforia.id/astro-blog
 * Version:     1.0.0-rc1
 * Author:      Webforia
 * Author URI:  https://webforia.id
 * Text Domain: astro_blog_domain
 */

if (!defined('ABSPATH')) {
    exit;
}

define('ASTRO_DYNAMIC_TEMPLATE', plugin_dir_path(__FILE__));
define('ASTRO_DYNAMIC_ASSETS', plugin_dir_url(__FILE__));

// Require the main plugin file
include plugin_dir_path(__FILE__) . 'function.php'; // function framework
include plugin_dir_path(__FILE__) . 'core/core-init.php'; // class framework
include plugin_dir_path(__FILE__) . 'widgets/elementor-init.php'; // class framework
include plugin_dir_path(__FILE__) . 'option/option-init.php';
include plugin_dir_path(__FILE__) . 'cpt/cpt-init.php';
include plugin_dir_path(__FILE__) . 'metabox/metabox-init.php';
include plugin_dir_path(__FILE__) . 'plugin.php'; // final class