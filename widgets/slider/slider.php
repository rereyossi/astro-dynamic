<?php
namespace Astro_Dynamic\Elementor;

use Astro_Dynamic\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Slider extends \Astro_Dynamic\Elementor_Base
{
    protected $post_type = 'slider';

    public function get_name()
    {
        return 'astro-dynamic-slider';
    }

    public function get_title()
    {
        return __('CPT Slider', 'rt_astro');
    }

    public function get_icon()
    {
        return 'ate-icon ate-slider';
    }

    public function get_categories()
    {
        return ['astro-dynamic'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_carousel(['carousel' => true]);

    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'post_id',
            [
                'label' => __('Select Slider', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => false,
                'options' => Helper::get_posts($this->post_type),

            ]
        );

        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings();
        $slider_id = $settings['post_id'];

 
        if( have_rows('slider_item', $slider_id) ): // check slider exits

            echo \Astro_Dynamic\HTML::before_slider(array(
                'id' => 'slider-'.$this->get_id(),
                'class' => ["rt-slider rt-slider--pagination-inner js-astro-slider"],
                'items-lg' => $settings['slider_item'],
                'items-md' => $settings['slider_item_tablet'],
                'items-sm' => $settings['slider_item_mobile'],
                'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
                'gap' => $settings['slider_gap'],
                'pagination' => ($settings['slider_pagination'] === 'yes') ? true : false,
                'loop' => $settings['slider_loop'],
                'autoplay' => $settings['slider_auto_play'],
            ));
            include ASTRO_DYNAMIC_TEMPLATE . 'widgets/slider/slider-view.php';

            echo \Astro_Dynamic\HTML::after_slider();
            
        endif;
        
    }
}
