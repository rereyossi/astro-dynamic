<?php
/** Classes */
include_once dirname(__FILE__) . '/class-notice.php';
include_once dirname(__FILE__) . '/class-html.php';
include_once dirname(__FILE__) . '/class-helper.php';
include_once dirname(__FILE__) . '/class-ajax.php';

add_action('elementor/widgets/widgets_registered', function () {
    include_once dirname(__FILE__) . '/class-elementor.php';
});

