 <div class="rt-slider__main owl-carousel">

        <?php while ( have_rows('slider_item', $slider_id) ) : the_row(); ?>

                <?php
                $link = get_sub_field('link');

                if( $link ){
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                }
                ?>
                <?php if($link): ?>
                <a href="<?php echo $link_url ?>" <?php $link_target?>>
                <?php endif ?>

                        <?php echo wp_get_attachment_image(get_sub_field('image'), 'full')?>

                <?php if($link): ?>
                </a>
                <?php endif ?>

        <?php endwhile;?> 

</div>