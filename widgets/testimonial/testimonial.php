<?php
namespace Astro_Dynamic\Elementor;

use Astro_Dynamic\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Testimonial extends \Astro_Dynamic\Elementor_Base
{
    protected $post_type = 'testimonial';
    protected $post_taxonomy = 'testimonial_category';

    public function get_name()
    {
        return 'astro-dynamic-testimonial';
    }

    public function get_title()
    {
        return __('CPT Testimonial', 'rt_astro');
    }

    public function get_icon()
    {
        return 'ate-icon ate-testimonial';
    }

    public function get_categories()
    {
        return ['astro-dynamic'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_option();
        $this->setting_pagination();

        $this->style_general();
        $this->style_body();
        $this->style_image();
        $this->style_name();
        $this->style_position();

        // extend global control
        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();

    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'astro_dynamic_domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_type,
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', 'astro_dynamic_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'astro_dynamic_domain'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'astro_dynamic_domain'),
                    'term' => __('Category', 'astro_dynamic_domain'),
                    'manually' => __('Manually Id', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'taxonomy',
            [
                'label' => __('taxonomy', 'astro_dynamic_domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_taxonomy,
            ]
        );

        $this->add_control(
            'term',
            [
                'label' => __('Category', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'default' => 'lastest',
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'term',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Testimonial', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'astro_dynamic_domain'),
                    'wp_post_views_count' => __('Most Viewer', 'astro_dynamic_domain'),
                    'comment_count' => __('Most Review', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'astro_dynamic_domain'),
                    'DESC' => __('DESC', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'astro_dynamic_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'astro_dynamic_domain'),
            ]
        );

        $this->end_controls_section();
    }

    /**
     * options
     */
    public function setting_option()
    {
        $this->start_controls_section(
            'setting_testimonial',
            [
                'label' => __('Options', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Avatar Size', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
                'condition' => [
                    'featured_image' => ['yes'],
                ],
            ]
        );


        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro_dynamic_domain'),
                    2 => __(2, 'astro_dynamic_domain'),
                    3 => __(3, 'astro_dynamic_domain'),
                    4 => __(4, 'astro_dynamic_domain'),
                    6 => __(6, 'astro_dynamic_domain'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'label_on' => __('On', 'astro_dynamic_domain'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'setting_position',
            [
                'label' => __('Position', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro_dynamic_domain'),
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->end_controls_section();

    }
    public function style_body()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .adc-testimonial__content',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'rt-domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'content_background',
            [
                'label' => __('Background Color', 'rt-domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__content,
                    {{WRAPPER}} .adc-testimonial__content::before' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'content_border_color',
            [
                'label' => __('Border Color', 'rt-domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__content,
                     {{WRAPPER}} .adc-testimonial__content::before' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'content_radius',
            [
                'label' => __('Border Radius', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__content' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }
    public function style_image()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Width', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'image_border_color',
                'selector' => '{{WRAPPER}} .adc-testimonial__thumbnail',
            ]
        );

        $this->add_responsive_control(
            'image_radius',
            [
                'label' => __('Border Radius', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__thumbnail' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }
    public function style_name()
    {
        $this->start_controls_section(
            'style_name',
            [
                'label' => __('Name', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'name_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__name' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'selector' => '{{WRAPPER}} .adc-testimonial__name',
            ]
        );

        $this->add_responsive_control(
            'name_spacing',
            [
                'label' => __('Name Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_position()
    {
        $this->start_controls_section(
            'style_position',
            [
                'label' => __('Position', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'position_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__position' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'position_typography',
                'selector' => '{{WRAPPER}} .adc-testimonial__position',
            ]
        );

        $this->add_responsive_control(
            'position_spacing',
            [
                'label' => __('Position Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-testimonial__testimonial' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/testimonial/testimonial-view',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
}
