<?php
namespace Astro_Dynamic\Elementor;

use Astro_Dynamic\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Portfolio extends \Astro_Dynamic\Elementor_Base
{
    protected $post_type = 'portfolio';
    protected $post_taxonomy = 'portfolio-category';

    public function get_name()
    {
        return 'astro-dynamic-portfolio';
    }

    public function get_title()
    {
        return __('CPT Portfolio', 'astro_dynamic_domain');
    }

    public function get_icon()
    {
        return 'ate-icon ate-portfolio';
    }

    public function get_categories()
    {
        return ['astro-dynamic'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options();
        $this->setting_pagination();

        $this->style_general();
        $this->style_body();
        $this->style_title();
        $this->style_meta();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.rt-post__readmore',
            'label' => 'Read More',
        ));

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();

    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'astro_dynamic_domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_type,
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', 'astro_dynamic_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'astro_dynamic_domain'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'astro_dynamic_domain'),
                    'category' => __('Category', 'astro_dynamic_domain'),
                    'manually' => __('Manually', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Categories', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Post', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'astro_dynamic_domain'),
                    'wp_post_views_count' => __('Most Viewer', 'astro_dynamic_domain'),
                    'comment_count' => __('Most Review', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'astro_dynamic_domain'),
                    'DESC' => __('DESC', 'astro_dynamic_domain'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'astro_dynamic_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'astro_dynamic_domain'),
            ]
        );

        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro_dynamic_domain'),
            ]
        );

        $this->add_control(
            'portfolio_style',
            [
                'label' => __('Style', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'grid',
                'options' => [
                    'grid' => 'Grid',
                    'transform' => 'Transform',
                ],
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro_dynamic_domain'),
                    2 => __(2, 'astro_dynamic_domain'),
                    3 => __(3, 'astro_dynamic_domain'),
                    4 => __(4, 'astro_dynamic_domain'),
                    6 => __(6, 'astro_dynamic_domain'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'label_on' => __('On', 'astro_dynamic_domain'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_off' => __('Off', 'astro_dynamic_domain'),
                'label_on' => __('On', 'astro_dynamic_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Featured Image', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->end_controls_section();
    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', 'astro_dynamic_domain'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro_dynamic_domain'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro_dynamic_domain'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro_dynamic_domain'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio__meta,
                    {{WRAPPER}} .adc-portfolio__body' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        // layout tab hover control
        $this->add_control(
            'general_background',
            [
                'label' => __('Background Options', 'astro_dynamic_domain'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->start_controls_tabs('general_background_tabs');

        $this->start_controls_tab(
            'general_background_normal',
            [
                'label' => __('Normal', 'rt-domain'),
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow',
                'selector' => '{{WRAPPER}} .adc-portfolio',

            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'general_background_tab_hover',
            [
                'label' => __('Hover', 'rt-domain'),
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow_hover',
                'selector' => '{{WRAPPER}} .adc-portfolio:hover',

            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .adc-portfolio',
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro_dynamic_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio' => 'border-radius: {{SIZE}}{{UNIT}};
                                                    overflow: hidden;',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_body()
    {

        $this->start_controls_section(
            'style_body',
            [
                'label' => __('Body', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'body_background',
            [
                'label' => __('Background Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'body_padding',
            [
                'label' => __('Spacing', 'astro_dynamic_domain'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'astro_dynamic_domain'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio__title' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'astro_dynamic_domain'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio__title:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .adc-portfolio__title',
            ]
        );

        $this->end_controls_section();
    }

    public function style_meta()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Category', 'astro_dynamic_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'meta_color',
            [
                'label' => __('Color', 'astro_dynamic_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .adc-portfolio__category a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'meta_typography',
                'selector' => '{{WRAPPER}} .adc-portfolio__category ',
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/portfolio/portfolio-view',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
    /* end class */
}
